// Declarative function using Array.map
function processArrayDeclarative(arr, callback) {
  return arr.map(callback);
}

// Sample Array of numbers
const numbers = [1, 2, 3, 4, 5];

const squareNumbers = processArrayDeclarative(numbers, (num) => num * num);
console.log(squareNumbers);

// Imperative function using a for loop
function processArrayImperative(arr, callback) {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(callback(arr[i]));
  }
  return result;
}

// Sample Array of numbers
const numbersArray = [1, 2, 3, 4, 5];

const result = processArrayImperative(numbers, (num) => num * num);
console.log(result);
