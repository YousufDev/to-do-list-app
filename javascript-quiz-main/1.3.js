// first approach using a recursive function
function flattenArray(arr) {
  let result = [];

  arr.forEach((element) => {
    if (Array.isArray(element)) {
      result = result.concat(flattenArray(element));
    } else {
      result.push(element);
    }
  });

  return result;
}

const nestedArray = [4, [[5], [6, [7], 8], 9, 10]];
const flatArray = flattenArray(nestedArray);

console.log(flatArray);

// second approach using the flat method available for arrays
const nestedArraydata = [4, [[5], [6, [7], 8], 9, 10]];
const result = nestedArraydata.flat(Infinity);

console.log(result);
