const object = {};

const handler = {
  get: function(target, prop) {
    // Always returns "404"
    return function () {
      return "404";
    };
  }
};

const manipulatedObject = new Proxy(object, handler);

// log the returning response
console.log(manipulatedObject.someProperty);
console.log(manipulatedObject.someMethod());
